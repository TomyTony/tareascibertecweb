﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    class SupplierRepository : Repository<Suppliers>, ISupplierRepository
    {
        public SupplierRepository(string connectionString) : base(connectionString)
        {

        }

        public Suppliers GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Suppliers>().Where(supplier => supplier.SupplierId.Equals(id)).First();
            }
        }

        public bool Update(Suppliers supplier)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("update Suppliers set CompanyName = @companyName, ContactName= @contactName, " +
                                                "ContactTitle=@contactTitle, City=@city, Country=@country, Phone=@phone, Fax=@fax " +
                                                "where SupplierId=@myId",
                                                new
                                                {
                                                    companyName = supplier.CompanyName,
                                                    contactName = supplier.ContactName,
                                                    contactTitle = supplier.ContactTitle,
                                                    city = supplier.City,
                                                    country= supplier.Country,
                                                    phone= supplier.Phone,
                                                    fax= supplier.Fax,
                                                    myId = supplier.SupplierId


                                                });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from Suppliers " +
                                                "where SupplierId=@myId",
                                                new { myId = id });

                return Convert.ToBoolean(result);

            }
        }




    }
}
