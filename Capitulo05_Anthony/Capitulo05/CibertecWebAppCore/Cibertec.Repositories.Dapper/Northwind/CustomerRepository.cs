﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using Dapper;

namespace Cibertec.Repositories.Dapper.Northwind
{
   public class CustomerRepository: Repository<Customers>, ICustomerRepository
    {
        //Se genera metodo Constructor porque esta heredando 
        public CustomerRepository(string connectionString): base(connectionString)
        {

        }

        public Customers GetById(String id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Customers>().Where(customer => customer.CustomerId.Equals(id)).First();
            }
        }

        public bool Update(Customers customer)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("update Customers set CompanyName = @company, ContactName= @contact, " +
                                                "City=@city, Country=@country, Phone=@phone " +
                                                "where CustomerID=@myId",
                                                new
                                                {
                                                    company = customer.CompanyName,
                                                    contact = customer.ContactName,
                                                    city = customer.City,
                                                    country = customer.Country,
                                                    phone = customer.Phone,
                                                    myId = customer.CustomerId
                                                });
                return Convert.ToBoolean(result);         
            }
        }

        public bool Delete(String id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from Customers " +
                                                "where CustomerID=@myId",
                                                new { myId = id });

                return Convert.ToBoolean(result);

            }
        }

    }
}
