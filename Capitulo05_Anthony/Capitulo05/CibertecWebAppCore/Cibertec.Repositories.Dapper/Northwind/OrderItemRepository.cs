﻿using Cibertec.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Repositories.Northwind;
using System.Data.SqlClient;
using System.Linq;
using Dapper.Contrib.Extensions;
using Dapper;

namespace Cibertec.Repositories.Dapper.Northwind
{
   public class OrderItemRepository: Repository<OrderItem>, IOrderItemRepository
    {
        //Se genera metodo Constructor porque esta heredando 
        public OrderItemRepository(string connectionString) : base(connectionString)
        {

        }

        public OrderItem GetById(String id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<OrderItem>().Where(OrderItem => OrderItem.OrderId.Equals(id)).First();
            }
        }

        public bool Update(OrderItem OrderItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("update OrderItem set UnitPrice = @unitprice, Quantity= @quantity, " +
                                                "Discount=@discount " +
                                                "where OrderId=@myId",
                                                new
                                                {
                                                    unitPrice = OrderItem.UnitPrice,
                                                    quantity = OrderItem.Quantity,
                                                    discount = OrderItem.Discount,
                                                    myId = OrderItem.OrderId

                                                });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from Orders " +
                                                "where OrderId=@myId",
                                                new { myId = id });

                return Convert.ToBoolean(result);

            }
        }

    }
}
