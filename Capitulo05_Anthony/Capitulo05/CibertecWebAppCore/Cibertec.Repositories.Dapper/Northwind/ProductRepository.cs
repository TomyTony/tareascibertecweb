﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    class ProductRepository : Repository<Products>, IProductRepository
    {

        //Se genera metodo Constructor porque esta heredando 
        public ProductRepository(string connectionString) : base(connectionString)
        {

        }

        public Products GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Products>().Where(product => product.ProductId.Equals(id)).First();
            }
        }

        public bool Update(Products product)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("update Products set ProductName = @productName, UnitPrice= @unitPrice, " +
                                                "QuantityPerUnit=@quantityPerUnit, Discontinued=@discontinued " +
                                                "where ProductId=@myId",
                                                new
                                                {
                                                    productName = product.ProductName,
                                                    unitPrice = product.UnitPrice,
                                                    quantityPerUnit = product.QuantityPerUnit,
                                                    discontinued = product.Discontinued,
                                                    myId = product.ProductId
                                                    

                                                });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from Products " +
                                                "where ProductId=@myId",
                                                new { myId = id });

                return Convert.ToBoolean(result);

            }
        }

    }
}
