﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Cibertec.Repositories.Dapper.Northwind
{
   public class OrderRepository: Repository<Orders>, IOrderRepository
    {
        //Se genera metodo Constructor porque esta heredando 
        public OrderRepository(string connectionString) : base(connectionString)
        {

        }

        public Orders GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Orders>().Where(order => order.OrderId.Equals(id)).First();
            }
        }

        public bool Update(Orders orders)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("update Orders set OrderDate = @orderDate, RequiredDate= @requiredDate, " +
                                                "ShippedDate=@shippedDate, ShipVia=@shipVia, Freight=@freight, ShipName=@shipName " +
                                                "where OrderId=@myId",
                                                new
                                                {
                                                    orderDate = orders.OrderDate,
                                                    requiredDate = orders.RequiredDate,
                                                    shippedDate = orders.ShippedDate,
                                                    shipVia = orders.ShipVia,
                                                    freight = orders.Freight,
                                                    shipName = orders.ShipName,
                                                    myId = orders.OrderId

                                                });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from Orders " +
                                                "where OrderId=@myId",
                                                new { myId = id });

                return Convert.ToBoolean(result);

            }
        }

    }
}

