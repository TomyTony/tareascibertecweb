﻿using Cibertec.Models;
using Cibertec.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.MVC.Controllers
{
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unit;

        public ProductController(IUnitOfWork unit)
        {
            //_unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ToString());
            _unit = unit;
        }


        // GET: Product
        public ActionResult Index()
        {
            return View(_unit.Products.GetList());
        }

        //GET
        public ActionResult Create()
        {
            return View();
        }

        //POST
        [HttpPost]
        public ActionResult Create(Products product)
        {
            if (ModelState.IsValid)
            {
                _unit.Products.Insert(product);
                return RedirectToAction("Index");
            }

            return View(product);
        }

        //GET
        public ActionResult Edit(int id)
        {
            return View(_unit.Products.GetById(id));
        }

        //POST
        [HttpPost]
        public ActionResult Edit(Products product)
        {
            if (_unit.Products.Update(product)) return RedirectToAction("Index");
            return View(product);

        }

        public ActionResult Delete(int id)
        {
            return View(_unit.Products.GetById(id));
        }

        //POST
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Products.Delete(id)) return RedirectToAction("Index");
            return View(_unit.Products.GetById(id));
        }

        public ActionResult Details(int id)
        {
            return View(_unit.Products.GetById(id));
        }

    }
}