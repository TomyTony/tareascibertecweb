﻿using Cibertec.Models;
using Cibertec.Repositories.Dapper.Northwind;
using Cibertec.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.MVC.Controllers
{
    public class OrderItemController : Controller
    {
        private readonly IUnitOfWork _unit;

        public OrderItemController(IUnitOfWork unit)
        {
            //_unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ToString());
            _unit = unit;
        }


        // GET: OrderItem
        public ActionResult Index()
        {
            return View(_unit.OrderItem.GetList());
        }

        //GET
        public ActionResult Create()
        {
            return View();
        }

        //POST
        [HttpPost]
        public ActionResult Create(OrderItem orderitem)
        {
            if (ModelState.IsValid)
            {
                _unit.OrderItem.Insert(orderitem);
                return RedirectToAction("Index");
            }

            return View(orderitem);
        }

        //GET
        public ActionResult Edit(int id)
        {
            return View(_unit.OrderItem.GetById(id));
        }

        //POST
        [HttpPost]
        public ActionResult Edit(OrderItem orderitem)
        {
            if (_unit.OrderItem.Update(orderitem)) return RedirectToAction("Index");
            return View(orderitem);

        }

        public ActionResult Delete(int id)
        {
            return View(_unit.OrderItem.GetById(id));
        }

        //POST
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.OrderItem.Delete(id)) return RedirectToAction("Index");
            return View(_unit.OrderItem.GetById(id));
        }

        public ActionResult Details(int id)
        {
            return View(_unit.OrderItem.GetById(id));
        }

    }
}