﻿using Cibertec.Models;
using Cibertec.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.MVC.Controllers
{
    public class OrderController : Controller
    {
        private readonly IUnitOfWork _unit;
        // GET: Order

        public OrderController(IUnitOfWork unit)
        {
            //_unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ToString());
            _unit = unit;
        }
        public ActionResult Index()
        {
            return View(_unit.Orders.GetList());
        }

        //GET
        public ActionResult Create()
        {
            return View();
        }

        //POST
        [HttpPost]
        public ActionResult Create(Orders order)
        {
            if (ModelState.IsValid)
            {
                _unit.Orders.Insert(order);
                return RedirectToAction("Index");
            }

            return View(order);
        }

        //GET
        public ActionResult Edit(int id)
        {
            return View(_unit.Orders.GetById(id));
        }

        //POST
        [HttpPost]
        public ActionResult Edit(Orders order)
        {
            if (_unit.Orders.Update(order)) return RedirectToAction("Index");
            return View(order);

        }

        public ActionResult Delete(int id)
        {
            return View(_unit.Orders.GetById(id));
        }

        //POST
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Orders.Delete(id)) return RedirectToAction("Index");
            return View(_unit.Orders.GetById(id));
        }

        public ActionResult Details(int id)
        {
            return View(_unit.Orders.GetById(id));
        }

    }
}