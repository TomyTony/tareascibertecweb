﻿using Cibertec.Models;
using Cibertec.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.MVC.Controllers
{
    public class SupplierController : Controller
    {
        private readonly IUnitOfWork _unit;

        public SupplierController(IUnitOfWork unit)
        {
            //_unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ToString());
            _unit = unit;
        }
        // GET: Supplier
        public ActionResult Index()
        {
            return View(_unit.Suppliers.GetList());
        }

        //GET
        public ActionResult Create()
        {
            return View();
        }

        //POST
        [HttpPost]
        public ActionResult Create(Suppliers supplier)
        {
            if (ModelState.IsValid)
            {
                _unit.Suppliers.Insert(supplier);
                return RedirectToAction("Index");
            }

            return View(supplier);
        }

        //GET
        public ActionResult Edit(int id)
        {
            return View(_unit.Suppliers.GetById(id));
        }

        //POST
        [HttpPost]
        public ActionResult Edit(Suppliers supplier)
        {
            if (_unit.Suppliers.Update(supplier)) return RedirectToAction("Index");
            return View(supplier);

        }

        public ActionResult Delete(int id)
        {
            return View(_unit.Suppliers.GetById(id));
        }

        //POST
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Suppliers.Delete(id)) return RedirectToAction("Index");
            return View(_unit.Suppliers.GetById(id));
        }

        public ActionResult Details(int id)
        {
            return View(_unit.Suppliers.GetById(id));
        }


    }
}